import unittest
import CreateTitle
import Testhelper 


class TestCreateTitle(unittest.TestCase):

    def test_usual_article(self):
        usual_article = 'Volvo released a new car with the following spec: V6 236HP. It will cost $22647 and going to be sold in New York only'
        self.assertEqual(CreateTitle.create_title(usual_article), 'Volvo released a new...')

    def test_article_len_24_and_kirill(self):
        article_len_24_kirill='Вашу мысль, мечтающую на'
        self.assertEqual(CreateTitle.create_title(article_len_24_kirill), 'Вашу мысль, мечтающую на')

    def test_article_len_25_latin_numbers(self):
        article_len_25_latin_numbers = 'Acb12345asd rtCC1234 0987'
        self.assertEqual(CreateTitle.create_title(article_len_25_latin_numbers), 'Acb12345asd rtCC1234 0987')

    def test_article_len_26_kirill_spec_symbol(self):
        article_len_26_kirill_spec_symbol = 'фывйыв!"№№%:,.;())))ПРОЛ:%'
        self.assertLessEqual(len(CreateTitle.create_title(article_len_26_kirill_spec_symbol)), 25)  

    def test_wrong_article(self):
        wrong_article = '!!@@@$%^&****())______)((           (*&^%$%^&*         '
        self.assertEqual(CreateTitle.create_title(wrong_article), 'ERROR: wrong article')

    def test_empty_article(self):
        empty_article=''
        self.assertEqual(CreateTitle.create_title(empty_article), 'ERROR: wrong article')

    def test_len_random_article(self):
        random_article = Testhelper.create_random_article()
        test_title = CreateTitle.create_title(random_article)
        assert (len(test_title) <= 25)
    
    def test_check_three_dots(self):
        random_article = Testhelper.create_random_article(random_len_of_article=15)
        test_title = CreateTitle.create_title(random_article)
        self.assertEqual(test_title[-3:], '...')

    def test_full_word(self):
        random_article = Testhelper.create_random_article(random_len_of_article=15)
        test_title = CreateTitle.create_title(random_article)
        title_without_dots = test_title[:-3]
        last_word=''
        for i in range(1, len(title_without_dots)):
            if title_without_dots[-i].isalpha():
                last_word+=title_without_dots[-i]
            else:
                break
        assert(last_word[::-1] in Testhelper.words)

if __name__ == "__main__":
  unittest.main()