import random

words = ['Any', 'language', 'can', 'be', 'used', 'to', 'complete', 'the', 'task', 'but', 'avoid', 'using', 
            'language', 'built-in', 'functions', 'just', 'low', 'level', 'code' ]

symbols = [' ', '.',' ', ',',' ', ':', ' ', ';', '-', ' ', ' ']

random_len_of_article = random.randint(0,len(words))

def create_random_article(random_len_of_article=random_len_of_article)->str:
    random_article = ''
    for i in range(random_len_of_article):
        random_word = (words[random.randint(0, len(words)-1)]) 
        random_article += (random_word + symbols[random.randint(0, len(symbols)-1)])
    print(f'Random article is {random_article}')
    return random_article

